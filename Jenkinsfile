node {
    properties([[
        $class:'jenkins.model.BuildDiscarderProperty',
        strategy:[
            $class:'LogRotator',
            numToKeepStr:'10'
        ]
    ]])

    stage "Checkout Source", {
        checkout scm
    }

    stage "Test", {
      sh './gradlew clean check'
      // Create folders and update timestamp on testReports if they exist
      sh 'mkdir -p build/test-results && touch build/test-results/*.xml'
      // Show test reports even if there were no changes
    }

    stage "Build Artifacts", {
        //Add metadata about build to the application.yml for the app to display at runtime
        sh "echo \"\n---\nappInfo:\n    version: '${getVersionNumber()}'\n    build: '${env.BUILD_NUMBER}'\n    commit: '${getGitHash()}'\" >> grails-app/conf/application.yml"

        sh './gradlew assemble groovyDoc'

        // publishHTML target:[allowMissing:false, alwaysLinkToLastBuild:true, keepAll:true, reportDir:'build/docs/groovydoc', reportFiles:'index.html', reportName:'GroovyDocs']

        //Cleanup
        cleanup()

        //Generate MD5 hash
        writeMd5()

        //Write last git commit log to file
        writeGitlog()

        //Save artifacts
        archiveArtifacts()
    }
}

def archiveArtifacts() {
    archive 'build/libs/*.war'
    archive 'build/libs/checksum.md5'
    archive 'build/libs/gitLog.txt'
}

def cleanup() {
    try {
        sh(script:'rm build/libs/*.original build/libs/*.md5 build/libs/gitLog.txt', returnStatus:false)
    } catch (ignore) {
    }
}

def writeMd5() {
    hash = sh(returnStdout:true, returnStatus:false, script:'md5sum build/libs/*.war').trim()
    writeFile file:'build/libs/checksum.md5', text:hash.split(" ")[0] + "\n"
}

def writeGitlog() {
    def gitLog = sh(script:'git log -n 1', returnStdout:true, returnStatus:false).trim()
    writeFile file:'build/libs/gitLog.txt', text:gitLog
}

def getVersionNumber() {
    sh(script:'./gradlew versionNumber | grep Version | awk \'{print \$2}\'', returnStdout:true, returnStatus:false).trim()
}

def getGitHash() {
    sh(script:'git rev-parse --short HEAD', returnStdout:true, returnStatus:false).trim()
}
