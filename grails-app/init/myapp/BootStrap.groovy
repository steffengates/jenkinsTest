package myapp

class BootStrap {

    def init = { servletContext ->
      new Person(name:'Steffen').save()
      new Person(name:'Zano').save()
    }
    def destroy = {
    }
}
